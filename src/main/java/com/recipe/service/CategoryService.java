package com.recipe.service;

import com.recipe.converter.CategoryConverter;
import com.recipe.dao.CategoryRepository;
import com.recipe.dto.CategoryDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {

    private final CategoryRepository categoryRepository;
    private final CategoryConverter categoryConverter;

    public CategoryService(CategoryRepository categoryRepository, CategoryConverter categoryConverter) {
        this.categoryRepository = categoryRepository;
        this.categoryConverter = categoryConverter;
    }

    public List<CategoryDto> fetchAll(){
        return categoryConverter.convert(categoryRepository.findAll());
    }

    public CategoryDto fetch(Long id) {
        return categoryConverter.convert(categoryRepository.findById(id).get());
    }

}
