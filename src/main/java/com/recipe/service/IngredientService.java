package com.recipe.service;

import com.recipe.converter.IngredientConverter;
import com.recipe.dao.IngredientRepository;
import com.recipe.dto.IngredientDto;
import com.recipe.entity.Ingredient;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IngredientService {

    private final IngredientConverter ingredientConverter;
    private final IngredientRepository ingredientRepository;

    public IngredientService(IngredientConverter ingredientConverter, IngredientRepository ingredientRepository) {
        this.ingredientConverter = ingredientConverter;
        this.ingredientRepository = ingredientRepository;
    }

    public List<IngredientDto> fetchAll() {
        return ingredientConverter.convert(ingredientRepository.findAll());
    }

    public void save(IngredientDto ingredientDto) {
        Long id = ingredientDto.getId();
        Ingredient ingredient = new Ingredient();
        if (id != null) {
            ingredient = ingredientRepository.getOne(id);
        }
        ingredient.setName(ingredientDto.getName());
        ingredientRepository.save(ingredient);
    }

}
