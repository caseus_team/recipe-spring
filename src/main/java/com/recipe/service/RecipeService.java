package com.recipe.service;

import com.recipe.converter.RecipeConverter;
import com.recipe.dao.CategoryRepository;
import com.recipe.dao.IngredientRepository;
import com.recipe.dao.RecipeIngredientRepository;
import com.recipe.dao.RecipeRepository;
import com.recipe.dto.IngredientDto;
import com.recipe.dto.RecipeDto;
import com.recipe.dto.RecipeIngredientDto;
import com.recipe.entity.Ingredient;
import com.recipe.entity.Recipe;
import com.recipe.entity.RecipeIngredient;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RecipeService {

    private final RecipeRepository recipeRepository;
    private final RecipeConverter recipeConverter;
    private final CategoryRepository categoryRepository;
    private final RecipeIngredientRepository recipeIngredientRepository;
    private final IngredientRepository ingredientRepository;

    public RecipeService(RecipeRepository recipeRepository,
                         RecipeConverter recipeConverter,
                         CategoryRepository categoryRepository,
                         RecipeIngredientRepository recipeIngredientRepository,
                         IngredientRepository ingredientRepository) {
        this.recipeRepository = recipeRepository;
        this.recipeConverter = recipeConverter;
        this.categoryRepository = categoryRepository;
        this.recipeIngredientRepository = recipeIngredientRepository;
        this.ingredientRepository = ingredientRepository;
    }

    public List<RecipeDto> fetchAll(Long catalogId) {
        return recipeConverter.convert(recipeRepository.findAllByCategoryId(catalogId));
    }

    public List<RecipeDto> fetchAll(List<IngredientDto> ingredientDtoList) {
        if (ingredientDtoList.isEmpty()) {
            return recipeConverter.convert(recipeRepository.findAll());
        }
        List<Ingredient> ingredients = ingredientDtoList.stream()
                                                        .map(IngredientDto::getId)
                                                        .map(ingredientRepository::getOne)
                                                        .collect(Collectors.toList());
        List<Recipe> recipes = recipeRepository.findAll()
                                               .stream()
                                               .filter(recipe -> recipe.getIngredients()
                                                                       .stream()
                                                                       .map(RecipeIngredient::getIngredient)
                                                                       .collect(Collectors.toList())
                                                                       .containsAll(ingredients))
                                               .collect(Collectors.toList());
        return recipeConverter.convert(recipes);
    }

    public RecipeDto fetch(Long id) {
        return recipeConverter.convert(recipeRepository.findById(id).get());
    }

    public void delete(Long id) {
        recipeRepository.deleteById(id);
    }

    public RecipeDto save(RecipeDto recipeDto) {
        Long id = recipeDto.getId();
        Recipe recipe = new Recipe();
        if (id != null) {
            recipe = recipeRepository.getOne(id);
        }
        recipe.setName(recipeDto.getName());
        recipe.setDescription(recipeDto.getDescription());
        recipe.setBriefDescription(recipeDto.getBriefDescription());
        if (recipeDto.getCategory() != null) {
            recipe.setCategory(categoryRepository.getOne(recipeDto.getCategory().getId()));
        }
        Recipe savedRecipe = recipeRepository.save(recipe);
        save(savedRecipe, recipeDto.getIngredients());
        return recipeConverter.convert(recipeRepository.getOne(savedRecipe.getId()));
    }

    private void save(Recipe recipe, List<RecipeIngredientDto> ingredients) {
        recipeIngredientRepository.saveAll(ingredients.stream()
                                                      .map(ingredientDto -> {
                                                          RecipeIngredient recipeIngredient = build(ingredientDto);
                                                          recipeIngredient.setRecipe(recipe);
                                                          return recipeIngredient;
                                                      })
                                                      .collect(Collectors.toList()));
    }

    private RecipeIngredient build(RecipeIngredientDto ingredientDto) {
        Long id = ingredientDto.getId();
        RecipeIngredient recipeIngredient = new RecipeIngredient();
        if (id != null) {
            recipeIngredient = recipeIngredientRepository.getOne(id);
        }
        recipeIngredient.setAmount(ingredientDto.getAmount());
        recipeIngredient.setIngredient(ingredientRepository.getOne(ingredientDto.getIngredient().getId()));
        return recipeIngredient;
    }

}
