package com.recipe.converter;

import com.recipe.dto.RecipeDto;
import com.recipe.entity.Recipe;
import org.springframework.stereotype.Service;

@Service
public class RecipeConverter implements AbstractConverter<Recipe, RecipeDto> {

    private final CategoryConverter categoryConverter;
    private final RecipeIngredientConverter recipeIngredientConverter;

    public RecipeConverter(CategoryConverter categoryConverter, RecipeIngredientConverter recipeIngredientConverter) {
        this.categoryConverter = categoryConverter;
        this.recipeIngredientConverter = recipeIngredientConverter;
    }

    @Override
    public RecipeDto convert(Recipe item) {
        RecipeDto recipeDto = new RecipeDto();
        recipeDto.setId(item.getId());
        recipeDto.setName(item.getName());
        recipeDto.setDescription(item.getDescription());
        recipeDto.setBriefDescription(item.getBriefDescription());
        if (item.getCategory() != null) {
            recipeDto.setCategory(categoryConverter.convert(item.getCategory()));
        }
        recipeDto.setIngredients(recipeIngredientConverter.convert(item.getIngredients()));
        return recipeDto;
    }
}
