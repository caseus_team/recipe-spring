package com.recipe.converter;

import com.recipe.dto.IngredientDto;
import com.recipe.entity.Ingredient;
import org.springframework.stereotype.Service;

@Service
public class IngredientConverter implements AbstractConverter<Ingredient, IngredientDto> {

    @Override
    public IngredientDto convert(Ingredient item) {
        IngredientDto ingredientDto = new IngredientDto();
        ingredientDto.setId(item.getId());
        ingredientDto.setName(item.getName());
        return ingredientDto;
    }
}
