package com.recipe.converter;

import java.util.List;
import java.util.stream.Collectors;

public interface AbstractConverter<S, T> {

    T convert(S item);

    default List<T> convert(List<S> collection) {
        return collection.stream().map(this::convert).collect(Collectors.toList());
    }

}
