package com.recipe.converter;

import com.recipe.dto.RecipeIngredientDto;
import com.recipe.entity.RecipeIngredient;
import org.springframework.stereotype.Service;

@Service
public class RecipeIngredientConverter implements AbstractConverter<RecipeIngredient, RecipeIngredientDto> {

    private final IngredientConverter ingredientConverter;

    public RecipeIngredientConverter(IngredientConverter ingredientConverter) {
        this.ingredientConverter = ingredientConverter;
    }

    @Override
    public RecipeIngredientDto convert(RecipeIngredient item) {
        RecipeIngredientDto recipeIngredientDto = new RecipeIngredientDto();
        recipeIngredientDto.setAmount(item.getAmount());
        recipeIngredientDto.setId(item.getId());
        recipeIngredientDto.setIngredient(ingredientConverter.convert(item.getIngredient()));
        return recipeIngredientDto;
    }
}
