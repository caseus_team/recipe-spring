package com.recipe.converter;

import com.recipe.dto.CategoryDto;
import com.recipe.entity.Category;
import org.springframework.stereotype.Service;

@Service
public class CategoryConverter implements AbstractConverter<Category, CategoryDto> {

    @Override
    public CategoryDto convert(Category item) {
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setId(item.getId());
        categoryDto.setDescription(item.getDescription());
        categoryDto.setName(item.getName());
        return categoryDto;
    }
}
