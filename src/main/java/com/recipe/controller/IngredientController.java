package com.recipe.controller;

import com.recipe.dto.IngredientDto;
import com.recipe.service.IngredientService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/ingredient")
public class IngredientController {

    private final IngredientService ingredientService;

    public IngredientController(IngredientService ingredientService) {
        this.ingredientService = ingredientService;
    }

    @RequestMapping("fetchAll")
    public List<IngredientDto> fetchAll() {
        return ingredientService.fetchAll();
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public void save(@RequestBody IngredientDto ingredient) {
        ingredientService.save(ingredient);
    }

}
