package com.recipe.controller;

import com.recipe.dto.CategoryDto;
import com.recipe.service.CategoryService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CategoryController {

    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @RequestMapping("/category/fetchAll")
    public List<CategoryDto> fetchAll() {
        return categoryService.fetchAll();
    }

    @RequestMapping("/category/fetch/{id}")
    public CategoryDto fetch(@PathVariable("id") Long id) {
        return categoryService.fetch(id);
    }

}
