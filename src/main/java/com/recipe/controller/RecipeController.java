package com.recipe.controller;

import com.recipe.dto.IngredientDto;
import com.recipe.dto.RecipeDto;
import com.recipe.service.RecipeService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/recipe")
public class RecipeController {

    private final RecipeService recipeService;

    public RecipeController(RecipeService recipeService) {
        this.recipeService = recipeService;
    }

    @RequestMapping("/fetchAll/{categoryId}")
    public List<RecipeDto> fetchAll(@PathVariable("categoryId") Long categoryId) {
        return recipeService.fetchAll(categoryId);
    }

    @RequestMapping("/delete/{id}")
    public void delete(@PathVariable("id") Long id) {
        recipeService.delete(id);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public RecipeDto save(@RequestBody RecipeDto recipe) {
        return recipeService.save(recipe);
    }

    @RequestMapping(value = "/fetchAll", method = RequestMethod.POST)
    public List<RecipeDto> fetchAll(@RequestBody List<IngredientDto> ingredients) {
        return recipeService.fetchAll(ingredients);
    }

    @RequestMapping("/fetch/{id}")
    public RecipeDto fetch(@PathVariable("id") Long id) {
        return recipeService.fetch(id);
    }

}
