package com.recipe.dao;

import com.recipe.entity.Ingredient;
import com.recipe.entity.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RecipeRepository extends JpaRepository<Recipe, Long> {

    List<Recipe> findAllByCategoryId(Long id);

    List<Recipe> findAllByIngredientsContains(List<Ingredient> ingredients);

}
